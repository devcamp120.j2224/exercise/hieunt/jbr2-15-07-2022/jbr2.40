public class App {
    public static void main(String[] args) throws Exception {
        Person person1 = new Person("Nam", "13 Le Do");
        Person person2 = new Person("Lan", "27 Le Loi");
        System.out.println("Person 1 là: " + person1);
        System.out.println("Person 2 là: " + person2);
        System.out.println(" ");
        Student student1 = new Student("Huy", "33 Le Lai", "Toan", 2021, 10000);
        Student student2 = new Student("Hoa", "41 Tran Phu", "Van", 2022, 12000);
        System.out.println("Student 1 là: " + student1);
        System.out.println("Student 2 là: " + student2);
        System.out.println(" ");
        Staff staff1 = new Staff("Ha", "56 Nguyen Du", "Tieu La", 55000);
        Staff staff2 = new Staff("Thanh", "67 Nguyen Trai", "Quang Trung", 45000);
        System.out.println("Staff 1 là: " + staff1);
        System.out.println("Staff 2 là: " + staff2);
    }
}
